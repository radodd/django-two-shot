from django.urls import path
from receipts.views import (
    receipt_list,
    receipt_create,
    show_expense_category_list,
    show_account_list,
    expense_category_create,
    account_create,
)

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", receipt_create, name="create_receipt"),
    path(
        "categories/",
        show_expense_category_list,
        name="category_list",
    ),
    path("accounts/", show_account_list, name="account_list"),
    path(
        "categories/create/", expense_category_create, name="create_category"
    ),
    path("accounts/create/", account_create, name="create_account"),
]
